/* --------------------------------------------------------------------------- 
 * Schéma de création de processus : création d'un fils, les deux processus
 * affichent simplement leur PID et PPID.
 * Auteur(s)    :
 * Groupe de TP :
 * --------------------------------------------------------------------------*/

#include <stdio.h>    /* Pour utiliser printf, perror, NULL... */
#include <stdlib.h>    /* Pour exit */
#include <unistd.h>    /* Pour fork, getpid, getppid */
#include <errno.h>
#include <sys/types.h>    /* Pour pid_t */
#include <sys/wait.h>    /* Pour wait */

extern int errno;    /* Modifiée en cas d'erreur */

/* Fonctions exécutant le code propre au père ou au fils */
void pere(void);
void fils(int nmax);

/* ---------------------------------------------------------------------------
 * Création d'un processus fils et exécution d'une fonction particulière
 * par chaque processus (père et fils).
 */
int main(int argc, char *argv[])
{
  pid_t ident;
  int n;

  //ident = fork();
  
  /* A partir de cette ligne, deux processus exécutent le code */
  printf("Cette ligne va être affichée deux fois\n");
    if(argc > 1)
    {
        n = strtol(argv[1],NULL,10);
        printf("Factorielle de %d :",n);
        fflush(stdout);
        fils(n);
    }
    

  return EXIT_SUCCESS;
}


/* Actions du processus père, regroupées dans une procédure. */
void pere(void)
{
  int pid1;
  printf("PERE --> PID = %d - PPID = %d\n", getpid(), getppid());
  
  pid1 = wait(NULL);
  
  
}

/* Actions du processus fils */
void fils(int nmax)
{
    int result = 1;
    int n =1;
  
    //printf("FILS --> PID = %d - PPID = %d\n", getpid(), getppid());
   
    while(n<nmax)
    {
        result = result * n;
        n++;
        
        if(fork() == 0)
        {
            // retourner en debut de boucle
        }
        else
        { 
            if(n==nmax)
            {
                    printf("%d \n", result*n);
                    fflush(stdout);
            }
            return;  
        }
    }
}



