/* --------------------------------------------------------------------------- 
 * Schéma de création de processus : création d'un fils, les deux processus
 * affichent simplement leur PID et PPID.
 * Auteur(s)    :
 * Groupe de TP :
 * --------------------------------------------------------------------------*/

#include <stdio.h>    /* Pour utiliser printf, perror, NULL... */
#include <stdlib.h>    /* Pour exit */
#include <unistd.h>    /* Pour fork, getpid, getppid */
#include <errno.h>
#include <sys/types.h>    /* Pour pid_t */
#include <sys/wait.h>    /* Pour wait */

extern int errno;    /* Modifiée en cas d'erreur */

/* Fonctions exécutant le code propre au père ou au fils */
void pere(void);
void fils(void);

/* ---------------------------------------------------------------------------
 * Création d'un processus fils et exécution d'une fonction particulière
 * par chaque processus (père et fils).
 */
int main(void)
{
  pid_t ident;

  ident = fork();
  
  /* A partir de cette ligne, deux processus exécutent le code */
  printf("Cette ligne va être affichée deux fois\n");

    if(ident == 0)
    {
        fils();
        execlp("xterm","-geometry 30x10-1-1",NULL);
    }
    else
    {
        ident = fork();
        if(ident == 0)
        {
            fils();
            execlp("xterm","-geometry 30x10-100-100",NULL);
        }
        else
            pere();
    }

  return EXIT_SUCCESS;
}


/* Actions du processus père, regroupées dans une procédure. */
void pere(void)
{
  int pid1, pid2;
  printf("PERE --> PID = %d - PPID = %d\n", getpid(), getppid());
  
  pid1 = wait(NULL);
  printf("FILS %d EST MORT\n",pid1);
  
  pid2 = wait(NULL);
  printf("FILS %d EST MORT\n",pid2);
}

/* Actions du processus fils */
void fils(void)
{
  printf("FILS --> PID = %d - PPID = %d\n", getpid(), getppid());
}