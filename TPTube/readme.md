
# Compte rendu TP-Tube
### *Guillaume Marmorat / Jeremie Chantharath*


## Question 1
Processus fils

    printf("On attend 2s\n");
    fflush(stdout);
    sleep(2);
    printf("Fils : j'écris 'Bonjour...' dans le tube\n");
    write(tube[1], "Bonjour...", 11);

Processus père

    printf("Père : je lis un message dans le tube\n");
    time(&temps0);
    read(tube[0], message, MAX);
    time(&temps1);
    printf("Père : j'ai lu '%s', j'ai attendu %ds.\n", message, (int)(temps1-temps0));

Trace

    Père : je lis un message dans le tube
    On attend 2s
    Fils : j'écris 'Bonjour...' dans le tube
    Père : j'ai lu 'Bonjour...', j'ai attendu 2s.

On voit bien que le processus père est bloqué dans le read jusqu'à la lecture.

## Question 2

Traces d'exécution

    Pere> on ecrit 'Bonjour'
    Fils1> on a lu Bonjour
    Fils1> on ecrit dans le tube
    Fils2> on a lu Bonjour
    Fils2> on ecrit dans le tube
    Pere> on a lu Bonjour

## Question 3
Ici les traces n'ont pas été effectuées à l'IUT, on a donc un utilisateur guillaume normal, un autre connecté sur le tty5 et un root sur tty6.
Le *cut* a été adapté.

Commande _who | cut -c 1-9_

    guillaume
    guillaume
    guillaume
    guillaume
    root

Commande _who | cut -c 1-9 | sort -u_

    guillaume
    root

Trace _redirigeWho_

    guillaume
    guillaume
    guillaume
    guillaume
    root

Trace _redirigeWho2_

    guillaume
    root

## Question 4
On essaie de créer le tube nommé

    if (mkfifo(tubeNom, S_IRUSR|S_IWUSR)!=0) {
       perror("Tube ");
    }else{
      printf("Tube nommé crée : %s\n", tubeNom);
    }

Ici la trace dépend de l'exécution.

Trace propre

    Tube : File exists
    Pere> on ecrit 'Bonjour'
    Fils1> on a lu Bonjour
    Fils1> on ecrit dans le tube
    Fils2> on a lu Bonjour
    Fils2> on ecrit dans le tube
    Pere> on a lu Bonjour

## Question Bonus
Dans cette question deux fils sont créés et écrivent chacun 10 fois dans le tube, le père lui, lit 10 fois dans le tube.
Ici aussi la trace est assez aléatoire.

    Tube : File exists
    Fils1>A ecrit
    Fils1>A ecrit
    Fils1>A ecrit
    Pere>On lis 10 fois
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils2Je suis le fils1Je suis le fils1
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils1Je suis le fils2Je suis le fils1
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils2Je suis le fils2Je suis le fils1
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils2Je suis le fils2Je suis le fils1
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils2Je suis le fils2Je suis le fils1
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils2Je suis le fils2Je suis le fils1
    Fils1>A ecrit
    Fils2>A ecrit
    Pere>Je suis le fils1Je suis le fils2Je suis le fils2Je suis le fils1
    Fils2>A ecrit
    Pere>Je suis le fils2Je suis le fils2Je suis le fils2Je suis le fils1
    Fils2>A ecrit
    Pere>Je suis le fils2Je suis le fils2Je suis le fils2Je suis le fils1
    Fils2>A ecrit
    Pere>Je suis le fils2Je suis le fils2Je suis le fils2Je suis le fils1
