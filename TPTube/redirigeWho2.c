/* ---------------------------------------------------------------------------
 * Question 1 du TP sur les tubes Unix.
 * 1 : on crée un tube puis un processus fils. Le fils envoie un message
 *     au père dans le tube.
 * Auteur(s) :
 * Groupe TP :
 * Entrées   : aucune
 * Sorties   : traces à l'écran
 * --------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */
#include <string.h>

#define MAX 64   /* Taille des tampons de communication */

int main(void)
{
   pid_t identCut;
   pid_t identWho;
   int tubeW_C[2];                 /* les deux descripteurs associés au tube */
   int tubeC_S[2];

   /* Création du tube */
   if (pipe(tubeW_C) != 0 || pipe(tubeC_S) !=0) {
      perror("Pipe creation failed.");
      return EXIT_FAILURE;
   }

   /* Création du fils */
   identCut = fork();

   if(identCut == 0){
     // stdout to tube
     dup2(tubeW_C[1], 1);
     close(tubeW_C[0]);
     close(tubeC_S[0]);
     close(tubeC_S[1]);


     execlp("who","who", NULL);

     return EXIT_SUCCESS;
   }
   else{
     identWho = fork();

     if(identWho > 0){
       // stdin from tube
       dup2(tubeC_S[0], 0);
       close(tubeC_S[1]);
       close(tubeW_C[0]);
       close(tubeW_C[1]);

       execlp("sort","sort","-u",NULL);

       return EXIT_SUCCESS;
     }
     else if(identWho == 0){

       dup2(tubeW_C[0], 0); // stdin from tube
       dup2(tubeC_S[1], 1); // stdout to tube
       close(tubeC_S[0]);
       close(tubeW_C[1]);

       execlp("cut","cut","-c","1-9", NULL);

       return EXIT_SUCCESS;
     }
   }
}
