#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */
#include <string.h>

#define MAX 64   /* Taille des tampons de communication */

int main(void)
{
   pid_t identF1;
   pid_t identF2;
   int tubeP_F1[2];                 /* les deux descripteurs associés au tube */
   int tubeF1_F2[2];
   int tubeF2_P[2];
   char message[MAX];           /* Une chaîne pour stocker les messages lus dans le tube */

   /* Création du tube */
   if (pipe(tubeP_F1) != 0 || pipe(tubeF1_F2) !=0 || pipe(tubeF2_P) !=0) {
      perror("Pipe creation failed.");
      return EXIT_FAILURE;
   }

   /* Création du fils */
   identF1 = fork();

   if(identF1 == 0){
     /* Fermeture Fils1 */
     close(tubeP_F1[1]);
     close(tubeF2_P[0]);
     close(tubeF2_P[1]);
     close(tubeF1_F2[0]);

     read(tubeP_F1[0], message, MAX);
     printf("Fils1> on a lu %s\n", message);
     fflush(stdout);

     printf("Fils1> on ecrit dans le tube\n");
     fflush(stdout);
     write(tubeF1_F2[1],message, strlen(message)+1);


     return EXIT_SUCCESS;
   }
   else{
     identF2 = fork();

     if(identF2 > 0){
       /* Fermeture Pere */
       close(tubeP_F1[0]);
       close(tubeF2_P[1]);
       close(tubeF1_F2[0]);
       close(tubeF1_F2[1]);

       printf("Pere> on ecrit 'Bonjour'\n");
       fflush(stdout);
       write(tubeP_F1[1], "Bonjour",8);

       read(tubeF2_P[0], message, MAX);
       printf("Pere> on a lu %s\n", message);
       fflush(stdout);

       return EXIT_SUCCESS;
     }
     else if(identF2 == 0){
       /* Fermeture Fils2 */
       close(tubeP_F1[0]);
       close(tubeP_F1[1]);
       close(tubeF1_F2[1]);
       close(tubeF2_P[0]);

       read(tubeF1_F2[0], message, MAX);
       printf("Fils2> on a lu %s\n", message);
       fflush(stdout);

       printf("Fils2> on ecrit dans le tube\n");
       fflush(stdout);
       write(tubeF2_P[1],message, strlen(message)+1);

       return EXIT_SUCCESS;
     }
   }
}
