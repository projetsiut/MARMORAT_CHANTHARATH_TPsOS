#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

#define MAX 64   /* Taille des tampons de communication */

int main(void)
{
   pid_t identF1;
   pid_t identF2;
   char message[MAX];           /* Une chaîne pour stocker les messages lus dans le tube */
   char * tubeNom = "./FIFO";
   int tube;

   /* Création du tube */
   if (mkfifo(tubeNom, S_IRUSR|S_IWUSR)!=0) {
      perror("Tube ");
   }else{
     printf("Tube nommé crée : %s\n", tubeNom);
   }

   /* Création du fils */
   identF1 = fork();

   if(identF1 == 0){ // ************************ Processus fils 1
     tube = open(tubeNom, O_RDWR);
     if (tube < 0) {
       perror("Ouverture du pipe _1 Fils1 ");
       exit(EXIT_FAILURE);
     }

     read(tube, message, MAX);
     printf("Fils1> on a lu %s\n", message);
     fflush(stdout);

     printf("Fils1> on ecrit dans le tube\n");
     fflush(stdout);
     write(tube, message, strlen(message)+1);

     close(tube);
     return EXIT_SUCCESS;
   }
   else{ // ************************ Processus pere
     identF2 = fork();

     if(identF2 > 0){
       tube = open(tubeNom, O_RDWR);
       if (tube < 0) {
         perror("Ouverture du pipe _1 Pere ");
         exit(EXIT_FAILURE);
       }

       printf("Pere> on ecrit 'Bonjour'\n");
       fflush(stdout);
       write(tube, "Bonjour",8);

      read(tube, message, MAX);
       printf("Pere> on a lu %s\n", message);
       fflush(stdout);

       close(tube);
       return EXIT_SUCCESS;
     }
     else if(identF2 == 0){ // ************************ Processus fils 2
       tube = open(tubeNom, O_RDWR);
       if (tube < 0) {
         perror("Ouverture du pipe _1 Fils2 ");
         exit(EXIT_FAILURE);
       }

       read(tube, message , MAX);
       printf("Fils2> on a lu %s\n", message);
       fflush(stdout);

       printf("Fils2> on ecrit dans le tube\n");
       fflush(stdout);
       write(tube, message, strlen(message)+1);

       close(tube);
       return EXIT_SUCCESS;
     }
   }
}
