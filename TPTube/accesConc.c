#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

#define MAX 64   /* Taille des tampons de communication */
#define TUBE_NAME "./FIFO"

/*
* Ecrit en 10 fois dans le pipe TUBE_NAME
*/
int brutWrite(char* message, int filsNb){
  int i;
  unsigned long str = strlen(message);
  int tube = open(TUBE_NAME,O_WRONLY);
  if (tube < 0) {
    perror("Erreur tube ");
  }

  for (i = 0; i < 10; i++) {
    write(tube, message, str);
    printf("Fils%d>A ecrit\n",filsNb);
  }

  close(tube);
  return EXIT_SUCCESS;
}

int main(int argc, char const *argv[]) {
  pid_t identF1, identF2;

  if (mkfifo(TUBE_NAME, S_IRUSR|S_IWUSR)!=0) {
     perror("Tube ");
  }else{
    printf("Tube nommé crée : %s\n", TUBE_NAME);
  }

  identF1 = fork();
  if (identF1 == 0) { // FIls 1 ecrit en boule
    brutWrite("Je suis le fils1",1);
  }else{
    identF2 = fork();
    if (identF2 == 0) { // Fils 2 ecrit en boucle
      brutWrite("Je suis le fils2",2);
    }else{ // Pere va lire
      char message[MAX];
      int i, tube;

      tube = open(TUBE_NAME, O_RDONLY);
      if(tube < 0){
        perror("Tube ");
      }

      printf("Pere>On lis 10 fois\n");
      for (i = 0; i < 10; i++) {
        read(tube, message,MAX);
        printf("Pere>%s\n",message);
        fflush(stdout);
      }

      close(tube);
      return EXIT_SUCCESS;
    }
  }
}
