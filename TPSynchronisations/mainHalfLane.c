
#define _GNU_SOURCE
#include <unistd.h> // For usleep
#include <pthread.h>
#include <errno.h>
#include <stdlib.h> // For rand
#include <semaphore.h>
#include <signal.h>
#include <limits.h>
#include <stdio.h>

#include <road.h>

#define NO_CAR -1

typedef struct{
    int lockedBy;
    pthread_mutex_t mutex;
} MTX;

typedef struct {
   int carId;
   int roadId; // 0: left -> right; 1: right -> left
} CarInfo;

    int lockedBy = NO_CAR;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* avancer(void* ci)
{
  CarInfo* carInfo = (CarInfo*) ci;
  const int carId = carInfo->carId;
  //const int roadId = carInfo->roadId;

  bool outOfRoad = false;

  while (!outOfRoad)
  {
    if(road_distToCross(carId) > road_minDist)
    {
      outOfRoad = !road_stepCar(carId);
      if(lockedBy == carId && road_distToCross(carId) == INT_MAX)
      {
        outOfRoad = !road_stepCar(carId);
        pthread_mutex_unlock(&mutex);
        lockedBy = NO_CAR;
      }
    }
    else
    {
      // Debug
      //printf("%d %d\n", carId, lockedBy);
      if(lockedBy == NO_CAR)
      {
        outOfRoad = !road_stepCar(carId);
        pthread_mutex_lock(&mutex);
        lockedBy = carId;
      }
      else if(lockedBy == carId)
      {
        outOfRoad = !road_stepCar(carId);
      }
    }

    usleep(3000);
  }

  road_removeCar(carId);

  return NULL;
}

void* createCars(void* unused)
{
   pthread_t carThread;
   const int nbCars = 300;
   CarInfo carsInfo[nbCars];
   const int minDelay = 300000;
   int delay;
   for (int i = 0; i < nbCars; i++) {
      delay = minDelay + rand()%(minDelay);
      usleep(delay);
      const int roadId = rand()>RAND_MAX/3 ? 0 : 1; // A bit more cars from left than from right
      carsInfo[i].carId = road_addCar(roadId);
      carsInfo[i].roadId = roadId;
      pthread_create(&carThread, NULL, avancer, &carsInfo[i]);
   }

   // Vu le délai entre le démarrage des threads et le fait qu'ils durent
   // a priori le même temps, je n'ai besoin de faire un join que sur le
   // dernier thread
   pthread_join(carThread, NULL);

   return NULL;
}


int main(int argc, const char *argv[])
{
   road_init(1);

   pthread_t createCarsThread;
   pthread_create(&createCarsThread, NULL, createCars, NULL);

   while (!road_isEscPressed() && pthread_tryjoin_np(createCarsThread, NULL) == EBUSY)
   {
      usleep(1000);
      road_refresh();
   }

   road_shutdown();

   return 0;
}
