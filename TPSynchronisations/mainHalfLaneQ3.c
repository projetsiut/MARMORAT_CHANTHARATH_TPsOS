
#define _GNU_SOURCE
#include <unistd.h> // For usleep
#include <pthread.h>
#include <errno.h>
#include <stdlib.h> // For rand
#include <semaphore.h>
#include <signal.h>
#include <limits.h>
#include <stdio.h>

#include <road.h>

#define NO_CAR -1
#define SEM_PSHARED 0
#define SEM_VALUE 3

typedef struct {
   int carId;
   int roadId; // 0: left -> right; 1: right -> left
} CarInfo;

sem_t semaphore[2];

void* avancer(void* ci)
{
  CarInfo* carInfo = (CarInfo*) ci;
  const int carId = carInfo->carId;
  const int roadId = carInfo->roadId;

  bool outOfRoad = false;
  bool gotToken = false;

  while (!outOfRoad)
  {
    if(road_distToCross(carId) > road_minDist)
    {
      if(road_distNextCar(carId) > road_minDist){
        outOfRoad = !road_stepCar(carId);
        if(gotToken && road_distToCross(carId) == INT_MAX)
        {
          outOfRoad = !road_stepCar(carId);
          sem_post(&semaphore[roadId]);
          gotToken = false;
        }
      }
    }
    else
    {
      // Debug
      //printf("Car %d token %d\n", carId, gotToken);
      if(gotToken == false)
      {
        outOfRoad = !road_stepCar(carId);
        sem_wait(&semaphore[roadId]);
        gotToken = true;
      }
      else if(gotToken)
      {
        outOfRoad = !road_stepCar(carId);
      }
    }

    usleep(3000);
  }

  road_removeCar(carId);

  return NULL;
}

void* createCars(void* unused)
{
   pthread_t carThread;
   const int nbCars = 300;
   CarInfo carsInfo[nbCars];
   const int minDelay = 300000;
   int delay;
   for (int i = 0; i < nbCars; i++) {
      delay = minDelay + rand()%(minDelay);
      usleep(delay);
      const int roadId = rand()>RAND_MAX/3 ? 0 : 1; // A bit more cars from left than from right
      carsInfo[i].carId = road_addCar(roadId);
      carsInfo[i].roadId = roadId;
      pthread_create(&carThread, NULL, avancer, &carsInfo[i]);
   }

   // Vu le délai entre le démarrage des threads et le fait qu'ils durent
   // a priori le même temps, je n'ai besoin de faire un join que sur le
   // dernier thread
   pthread_join(carThread, NULL);

   return NULL;
}

void* switchLane(void* unused){
  bool roadToLock = false;
  int i;

  while (!road_isEscPressed()) {
    if (roadToLock) {
      for (i = 0; i < SEM_VALUE; i++) {
        sem_wait(&semaphore[0]);

      }
      printf("Lock 0\n");
      fflush(stdout);
      usleep(500000);
      for (i = 0; i < SEM_VALUE; i++) {
        sem_post(&semaphore[1]);
      }
      printf("Unlock 1\n");
      fflush(stdout);
      roadToLock = !roadToLock;
    }
    else
    {
      for (i = 0; i < SEM_VALUE; i++) {
        sem_wait(&semaphore[1]);
      }
      printf("Lock 1\n");
      fflush(stdout);
      usleep(500000);
      for (i = 0; i < SEM_VALUE; i++) {
        sem_post(&semaphore[0]);
      }
      printf("Unlock 0\n");
      fflush(stdout);
      roadToLock = !roadToLock;
    }
    sleep(1);
  }
  return NULL;
}


int main(int argc, const char *argv[])
{
  pthread_t createCarsThread;
  pthread_t laneSwitcher;

  sem_init(&semaphore[0],SEM_PSHARED,0);
  sem_init(&semaphore[1],SEM_PSHARED,SEM_VALUE);

   road_init(1);


   pthread_create(&createCarsThread, NULL, createCars, NULL);
   pthread_create(&laneSwitcher, NULL, switchLane, NULL);

   while (!road_isEscPressed() && pthread_tryjoin_np(createCarsThread, NULL) == EBUSY)
   {
      usleep(1000);
      road_refresh();
   }

   road_shutdown();
   sem_destroy(&semaphore[0]);
   sem_destroy(&semaphore[1]);

   return 0;
}
