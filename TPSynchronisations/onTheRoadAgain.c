
#define _GNU_SOURCE
#include <unistd.h> // For usleep
#include <pthread.h>
#include <stdlib.h> // For rand
#include <signal.h> // For signals
#include <sys/time.h> // For setitimer
#include <stdio.h>

#include "road.h"

int main(int argc, const char *argv[])
{
   road_init(0);

   int carId1 = road_addCar(0);
   if (carId1 == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }

   int carId2 = road_addCar(1);
   if (carId2 == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }

   while (!road_isEscPressed())
   {
      road_stepCar(carId1);
      road_stepCar(carId2);
      road_refresh();
   }

   road_shutdown();

   return 0;
}

