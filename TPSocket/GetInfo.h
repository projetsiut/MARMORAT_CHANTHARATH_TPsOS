/* ----------------------------------------------------------------------------------
 * Accès à quelques informations matérielles de la machine : processeurs(s), mémoire
 * Auteur     : Damien Genthial
 * Création   : 02 décembre 2014
 * Exports    : getCPUInfo, getMemoryInfo, getSystemInfo
 */

#ifndef GETINFO_H
#define GETINFO_H

#ifndef GETINFO_C
#define PUBLIC extern
#else
#define PUBLIC
#endif

#include <sys/utsname.h>

typedef struct {
    int nbProc;
    char model_name[256]; // Modèle de CPU
} CPUInfo;

PUBLIC CPUInfo getCPUInfo();
PUBLIC int getMemorySize();
PUBLIC void getSystemInfo(struct utsname *);

#undef PUBLIC
#endif	/* GETINFO_H */
