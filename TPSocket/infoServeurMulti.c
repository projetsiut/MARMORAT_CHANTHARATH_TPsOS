#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <pthread.h>
#include <signal.h>

#include "GetInfo.h"
#include "Reseau.h"

bool exitServ = false;

void* clientHandler(void* socket){
  int msgSock = *((int*)socket);
  int err;
  char* bf = (char*)calloc(BUFF_SIZE,sizeof(char));
  char* err_inc = (char*)malloc((BUFF_SIZE+20)*sizeof(char));
  char* cpu = (char*)malloc(BUFF_SIZE*sizeof(char));
  char* mem = (char*)malloc(BUFF_SIZE*sizeof(char));
  char* sys = (char*)malloc(BUFF_SIZE*sizeof(char));

  // On recupere les différentes info sur le serveur
  CPUInfo cpuInf = getCPUInfo();
  int memSize = getMemorySize();
  struct utsname sysInfo;
  getSystemInfo(&sysInfo);

  sprintf(cpu,"%d Coeur, %s", cpuInf.nbProc, cpuInf.model_name);
  sprintf(mem,"%d kB", memSize);
  sprintf(sys,"%s %s %s %s %s",sysInfo.nodename, sysInfo.sysname, sysInfo.machine, sysInfo.release, sysInfo.version);

  do {
    err = read(msgSock,bf,BUFF_SIZE);
    if(err == -1){ // Cas de l'erreur de lecture
      printf("Erreur de lecture\n");
      break;
    }

    if(strcmp(bf,"") == 0){ // Cas du client qui quitte
      printf("\nClient déconnecté\n");
      break;
    }

    printf("Sock>%d | Entrée:%10s ",msgSock ,bf);

    if(strcmp(bf,"cpu") == 0){
      printf("> Réponse cpu info\n");
      write(msgSock,cpu, strlen(cpu)+1);
    }else if(strcmp(bf,"mem") == 0){
      printf("> Réponse mem info\n");
      write(msgSock, mem, strlen(mem)+1);
    }else if(strcmp(bf,"sys") == 0){
      printf("> Réponse sys info\n");
      write(msgSock, sys, strlen(sys)+1);
    }else{
      printf("> Réponse commande inconnue\n");
      sprintf(err_inc, "Commande inconnue %s", bf);
      write(msgSock,err_inc, strlen(err_inc)+1);
    }

    memset(bf, 0, strlen(bf)+1); // Reinitialisation du buffer
    usleep(20000);
  } while(msgSock != -1);

  printf("Fermeture du socket %d\n",msgSock);
  fflush(stdout);

  close(msgSock);
  free(bf);
  free(cpu);
  free(mem);
  free(sys);

  return NULL;
}

static void interruptHandler(int sig)
{
  exitServ = true;
}

int main(int argc, char const *argv[]) {
  int socketNum,msgSock;
  pthread_t handler;

  // Pour gerer la fermeture du serveur
  struct sigaction sa;
  sa.sa_handler = interruptHandler;
  sa.sa_flags = 0;
  sigaction(SIGINT, &sa, NULL);

  // Creation du socket
  socketNum = socketServer(2223,TCP);
  printf("Serveur ouvert, port:%d\n", 2223);

  do {
    msgSock = accept(socketNum, NULL, NULL); // On redirige chaque nouveau client vers un thread
    printf("Client connecté, socket:%d\n", msgSock);
    pthread_create(&handler, NULL, clientHandler, (void*)&msgSock);
  } while(!exitServ);

  printf("Fermeture du serveur.\n");
  close(socketNum);

  return 0;
}
