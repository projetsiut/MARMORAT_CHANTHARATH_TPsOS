#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "Reseau.h"

int main(int argc, char const *argv[]) {
  const char* server = "gigondas";
  int port = 2222;

  if(argc > 2){
    port = atoi(argv[2]);
    server = argv[1];
  }

  int socketNum = socketClient(server,port, TCP);
  char *str, *ptEnd;
  str = (char*)malloc(BUFF_SIZE*sizeof(char));

  do {
    fgets(str,BUFF_SIZE,stdin);
    if(strlen(str)-1 <= 0){
      write(socketNum,"\0",1);
      break;
    }
    ptEnd = strchr(str,'\n');
    *(ptEnd) = '\0';

    write(socketNum,str,strlen(str)); // On envoie au serveur la commande saisie
    read(socketNum,str,BUFF_SIZE); // On recupere les info en retour du serveur
    printf("%s\n",str); // On affiche le retour
  } while(1);

  free(str);
  close(socketNum);

  return 0;
}
