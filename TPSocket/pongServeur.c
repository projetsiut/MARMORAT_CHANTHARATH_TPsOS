#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "Reseau.h"

int main(int argc, char const *argv[]) {
  int socketNum,msgSock,err;
  char* bf = (char*)calloc(BUFF_SIZE,sizeof(char));
  char pong[] = "PONG", err_inc[] = "Commande inconnue";


  socketNum = socketServer(2223,TCP);
  msgSock = accept(socketNum, NULL, NULL);
  printf("Client connecté:%d\n", msgSock);

  do {
    err = read(msgSock,bf,BUFF_SIZE);
    if(err == -1){ // Cas de l'erreur de lecture
      printf("Erreur lecture\n");
      break;
    }

    if(strcmp(bf,"") == 0){ // Cas du client qui quitte
      printf("Fermeture du client\n");
      break;
    }

    if(strcmp(bf,"PING") == 0){
      printf("Entree:%s\n",bf );
      printf("Repond:%s\n",pong);
      write(msgSock,pong, strlen(pong)+1);
    }else{
      write(msgSock,err_inc, strlen(err_inc)+1);
    }
    usleep(20000);
  } while(msgSock != -1);

  printf("Fermeture\n");
  fflush(stdout);

  close(msgSock);
  close(socketNum);
  free(bf);

  return 0;
}
