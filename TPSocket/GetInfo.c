/* ---------------------------------------------------------------------------------
 * Accès à quelques informations matérielles de la machine : processeurs(s), mémoire
 * Auteur     : Damien Genthial
 * Création   : 02 décembre 2014
 * Exports    : getCPUInfo, getMemoryInfo, getSystemInfo
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/utsname.h>

#define GETINFO_C
#include "GetInfo.h"

/* ------- Procédures publiques ------- */

/* Récupération des informations CPU (modèle et nombre de processeurs) par parcours du
 * pseudo-fichier /proc/cpuinfo
 * Retourne une structure CPUInfo */
CPUInfo getCPUInfo()
{
    CPUInfo res;
    FILE* f = fopen("/proc/cpuinfo", "r");
    char ligne[256];
    res.nbProc = 0;
    while (fgets(ligne, 256, f) != NULL) {
        if (strstr(ligne, "model name") == ligne) {
            res.nbProc += 1;
            char* debut = strchr(ligne, ':') + 2; // sauter le ':' et le ' '
            char* rc = strrchr(debut, '\n');
            if (rc != NULL) *rc = '\0';
            strcpy(res.model_name, debut);
        }
    }
    fclose(f);
    return res;
}

/* Récupération de la taille mémoire par parcours du pseudo-fichier /proc/meminfo
 * Retourne la taille mémoire en kB */
int getMemorySize()
{
    int res = 0;
    FILE* f = fopen("/proc/meminfo", "r");
    char ligne[256];
    bool trouve = false;
    while ((fgets(ligne, 256, f) != NULL) && ! trouve) {
        if (strstr(ligne, "MemTotal") == ligne) {
            sscanf(ligne + 10, "%d", &res);
            trouve = true;
        }
    }
    fclose(f);
    return res;
}

/* Récupération des informations sur le système d'exploitation. On se contente ici d'appeler la
 * fonction uname */
void getSystemInfo(struct utsname * info) { uname(info); }
