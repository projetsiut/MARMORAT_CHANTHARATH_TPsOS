#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>


#include "GetInfo.h"
#include "Reseau.h"

int main(int argc, char const *argv[]) {
  int socketNum,msgSock,err;
  char* bf = (char*)calloc(BUFF_SIZE,sizeof(char));
  char* err_inc = (char*)malloc((BUFF_SIZE+20)*sizeof(char));
  char* cpu = (char*)malloc(BUFF_SIZE*sizeof(char));
  char* mem = (char*)malloc(BUFF_SIZE*sizeof(char));
  char* sys = (char*)malloc(BUFF_SIZE*sizeof(char));

  // On recupere les différentes info sur le serveur
  CPUInfo cpuInf = getCPUInfo();
  sprintf(cpu,"%d Coeur, %s", cpuInf.nbProc, cpuInf.model_name);

  int memSize = getMemorySize();
  sprintf(mem,"%d kB", memSize);

  struct utsname sysInfo;
  getSystemInfo(&sysInfo);
  sprintf(sys,"%s %s %s %s %s",sysInfo.nodename, sysInfo.sysname, sysInfo.machine, sysInfo.release, sysInfo.version);

  printf("Serveur info:\n %s \n %s \n %s \n",sys, cpu, mem);

  // Creation du socket et attente d'un client
  socketNum = socketServer(2223,TCP);
  msgSock = accept(socketNum, NULL, NULL);
  printf("Client connecté, socket:%d\n", msgSock);

  do {
    err = read(msgSock,bf,BUFF_SIZE);
    if(err == -1){ // Cas de l'erreur de lecture
      printf("Erreur de lecture\n");
      break;
    }

    if(strcmp(bf,"") == 0){ // Cas du client qui quitte
      printf("\nClient déconnecté\n");
      break;
    }

    printf("Entrée:%10s ", bf);

    if(strcmp(bf,"cpu") == 0){
      printf("> Réponse cpu info\n");
      write(msgSock,cpu, strlen(cpu)+1);
    }else if(strcmp(bf,"mem") == 0){
      printf("> Réponse mem info\n");
      write(msgSock, mem, strlen(mem)+1);
    }else if(strcmp(bf,"sys") == 0){
      printf("> Réponse sys info\n");
      write(msgSock, sys, strlen(sys)+1);
    }else{
      printf("> Réponse commande inconnue\n");
      sprintf(err_inc, "Commande inconnue %s", bf);
      write(msgSock,err_inc, strlen(err_inc)+1);
    }

    memset(bf, 0, strlen(bf)+1); // Reinitialisation du buffer
    usleep(20000);
  } while(msgSock != -1);

  printf("Fermeture\n");
  fflush(stdout);

  close(msgSock);
  close(socketNum);
  free(bf);
  free(cpu);
  free(mem);
  free(sys);

  return 0;
}
