/* ---------------------------------------------------------------------------
 * Test du module GetInfo
 * Auteur     : Damien Genthial
 * Création   : 02 décembre 2014
 * Entrées    : aucune 
 * Sorties    : traces à l'écran 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/utsname.h>
#include "GetInfo.h"

int main(int nbArgs, char* arg[])
{
    CPUInfo cpu = getCPUInfo();
    if (cpu.nbProc == 1)
        printf("1 processeur %s\n", cpu.model_name);
    else
        printf("%d processeurs %s\n", cpu.nbProc, cpu.model_name);

    printf("Taille mémoire : %d kB\n", getMemorySize());

    struct utsname sys;
    getSystemInfo(&sys);
    printf("Système sysname = %s\n", sys.sysname);
    printf("Système nodename = %s\n", sys.nodename);
    printf("Système release = %s\n", sys.release);
    printf("Système version = %s\n", sys.version);
    printf("Système machine = %s\n", sys.machine);

    return EXIT_SUCCESS;
}

