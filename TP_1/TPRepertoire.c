#include <dirent.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "TPRepertoire.h"

void main(int nbArgs, char *args[])
{
    int i = 1;

    if (nbArgs > 1) {
        while (i < nbArgs) {
            afficheRepertoire(args[i]);
            i++;
        }
    } else {
        afficheCourant();
    }
}

void afficheRepertoire(char *dirName)
{
    DIR *rep;
    struct dirent *currentFile;
    int repCount;
    char **tabToSort;

    rep = opendir(dirName);
    // On compte le nombre d'element
    if (rep != NULL)
        repCount = countContent(rep);

    // Allocation dynamique de nombre d'element * taille d'un pointeur sur char
    tabToSort = malloc(repCount * sizeof(char *));
    if (tabToSort == NULL) {
        exit(1);
    }

    currentFile = readdir(rep);
    for (char i = 0; i < repCount; i++) {
        // Allocation dynamique avec +1 pour \0, copie dans l'espace alloué
        tabToSort[i] = malloc(strlen(currentFile->d_name) + 1);
        strcpy(tabToSort[i], currentFile->d_name);
        currentFile = readdir(rep);
    }

    // Tri du tableau avec strcmp comme fonction
    qsort(tabToSort, repCount, sizeof(char *), compare);

    // Affichage du tableau
    for (int i = 0; i < repCount; i++) {
        printf("%s\n", tabToSort[i]);
    }

    for (int j = 0; j < repCount; j++) {
        free(tabToSort[j]);
    }
    free(tabToSort);
    closedir(rep);
}

void afficheCourant()
{
    DIR *rep;
    struct dirent *currentFile;

    rep = opendir(".");
    if (rep != NULL) {
        currentFile = readdir(rep);
        while (currentFile != NULL) {
            printf("%d %s\n", currentFile->d_ino, currentFile->d_name);
            currentFile = readdir(rep);
        }
        closedir(rep);
    }
}

int compare(const void* str1, const void* str2)
{
    return strcmp(*((char**) str1), *((char**) str2));
}

int countContent(DIR * rep)
{
    // INIT
    int result = 0;
    struct dirent *currentFile;

    // BEGIN
    rewinddir(rep);
    currentFile = readdir(rep);
    while (currentFile != NULL) {
        result++;
        currentFile = readdir(rep);
    }

    // END
    rewinddir(rep);
    return result;
}
