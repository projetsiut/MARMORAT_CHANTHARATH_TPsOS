#ifndef _TPREPERTOIRE_H
#define _TPREPERTOIRE_H

  void afficheCourant();
  void afficheRepertoire(char* dirName);
  int countContent(DIR* rep);
  int compare(const void* str1, const void* str2);


#endif
