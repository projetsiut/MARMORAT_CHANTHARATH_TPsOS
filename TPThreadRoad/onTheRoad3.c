/* ---------------------------------------------------------------------------
 * Programme affichant une voiture qui parcours une route
 * Auteur(s)  : Jeremie Chantharath - Guillaume Marmorat
 * Groupe TP  : 2J
 * Entrées    : Aucune
 * Sorties    : Affichage d'une fenêtre graphique
 * Avancement : Une thread qui créer les voitures, une thread par voiture,
 */

#include "road.h"

#define _GNU_SOURCE  // For pthread_tryjoin_np
#include <unistd.h>  // For usleep
#include <pthread.h> // For threading in general
#include <errno.h>   // For EBUSY
#include <stdlib.h>  // For EXIT_FAILURE
#include <stdio.h>   // Just in case

void* avancer(void* carId);
void* buildCar(void* ptNbCar);

int main(int argc, const char *argv[])
{
   road_init();

   //Génère un nombre de voiture aléatoire entre 1 et 10
   int nbCar = 9;

   //Créer le thread qui créera les threads ajoutant les voitures sur la route et les faisant avancer
   pthread_t newCar;
   if(pthread_create(&newCar, NULL, buildCar,(void*)&nbCar) == 0)
    {
            printf("Create, build\n");
    }

    //Le thread principal refresh la route tant qu'il y a des voitures qui roule ou que l'utilisateur n'appuie pas sur entrée
   while (!road_isEscPressed())
   {
      usleep(1000);
      road_refresh();
      if(pthread_tryjoin_np(newCar, (void**)NULL) == 0)
        break;
   }

   road_shutdown();

   return 0;
}


void* avancer(void* carId)
{
    int* car = carId;
    while(!road_isEscPressed() && road_stepCar(*car))
    {
        usleep(1000);
    }

    road_removeCar(*car);
}

void* buildCar(void* ptNbCar)
{
    // On récupère le nombre de voiture a créer
    int* nbCar = ptNbCar;
    int carId[*nbCar];

    pthread_t newCar;
    for(*nbCar; *nbCar != 0; *nbCar -=1)
   {
       carId[*nbCar-1] = road_addCar();
       if(pthread_create(&newCar, NULL, avancer,(void*)&carId[*nbCar-1]) == 0)
       {
           printf("Create, %d\n", carId[*nbCar-1] );
       }
       // sert à créer un décalage entre l'ajoute de chaque voiture de sorte à ce qu'elle ne soit pas superposées
       srand( time (NULL));
       usleep(rand() % (300000-20000)+20000);
       //usleep(10000);


    }
    pthread_join(newCar, NULL);
}
