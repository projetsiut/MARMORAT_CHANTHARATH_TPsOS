#include <stdbool.h>

#ifndef ROAD_H
#define ROAD_H


/** Initialise la bibliothèque Allegro, et l'affichage */
int road_init();

/** Ferme proprement l'affichage et la bibliothèque Allegro */
void road_shutdown();

/** Met à jour l'affichage, notament les voitures
 * Ne peut être appelée qu'à partir du thread principal.*/
void road_refresh();

/** Ajoute une voiture.
 * Retourne l'identifiant de la voiture ajoutée,
 * identifiant à utiliser pour appeler road_stepCar et road_removeCar.
 * Retourne -1 si le nombre maximum de voiture a été atteint.
 */
int road_addCar();

/** Supprime la voiture ayant l'identifiant passé en paramètre */
void road_removeCar(int id);

/** Fait avancer la voiture ayant l'identifiant passé en paramètre.
 * Retourne vrai tant que la voiture n'a pas dépassé le bout de la route. */
bool road_stepCar(int id);


/** Retourne vrai si la touche Escape a été pressée */
bool road_isEscPressed();

/** Retourne vrai si la touche P a été pressée */
bool road_isPPressed();

#endif

