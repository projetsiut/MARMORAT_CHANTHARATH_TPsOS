/* ---------------------------------------------------------------------------
 * Programme affichant une voiture qui parcours une route
 * Auteur(s)  : 
 * Groupe TP  : 
 * Entrées    : Aucune
 * Sorties    : Affichage d'une fenêtre graphique
 * Avancement : <Où en êtes vous ? Qu'est-ce qui fonctionne ?>
 */

#include "road.h"

#define _GNU_SOURCE  // For pthread_tryjoin_np
#include <unistd.h>  // For usleep
#include <pthread.h> // For threading in general
#include <errno.h>   // For EBUSY
#include <stdlib.h>  // For EXIT_FAILURE
#include <stdio.h>   // Just in case

void* avancer(void* carId);

int main(int argc, const char *argv[])
{
   road_init();
   
    pthread_t myThread;
   int carId = road_addCar();
    
   if (carId == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }
   
   pthread_create(&myThread, NULL, avancer,(void*)&carId);

   while (!road_isEscPressed())
   {
      usleep(1000);
      road_refresh();
   }
   

   road_shutdown();

   return 0;
}


void* avancer(void* carId)
{
    int* car = carId;
    while(!road_isEscPressed() && road_stepCar(*car))
    {   
        usleep(1000);
    }
    
    road_removeCar(*car);
}


