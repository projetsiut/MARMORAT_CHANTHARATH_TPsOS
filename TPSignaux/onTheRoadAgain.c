
#define _GNU_SOURCE
#include <unistd.h> // For usleep
#include <pthread.h>
#include <stdlib.h> // For rand
#include <signal.h> // For signals
#include <sys/time.h> // For setitimer
#include <stdio.h>

#include "road.h"

void* avancer(void* rdNum)
{
    int* road = rdNum;
    int carId = road_addCar(*road);
    while(!road_isEscPressed() && road_stepCar(carId))
    {
        usleep(1000);
    }
    road_removeCar(carId);
}

static void handler(int sig)
{
  int road0 = 0, road1 = 1;

  pthread_t newcar1, newcar2;
  pthread_create(&newcar1, NULL, avancer, &road0);
  pthread_create(&newcar2, NULL, avancer, &road1);
  alarm(2);
}

int main(int argc, const char *argv[])
{
   road_init(0);

   int carId1 = road_addCar(0);
   if (carId1 == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }

   int carId2 = road_addCar(1);
   if (carId2 == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_flags = 0;
    sigaction(SIGALRM, &sa, NULL);
    //sigaction(SIGINT, &sa, NULL);

    alarm(2);

   while (!road_isEscPressed())
   {
      road_stepCar(carId1);
      road_stepCar(carId2);
      road_refresh();
   }

   road_shutdown();

   return 0;
}
